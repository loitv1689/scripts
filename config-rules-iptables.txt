[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 1024 -j ACCEPT
[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 1025 -j ACCEPT
[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 1026 -j ACCEPT
[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 1027 -j ACCEPT
[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 1028 -j ACCEPT
[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 1029 -j ACCEPT
[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 1030 -j ACCEPT
[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 3306 -j ACCEPT
[0:0] -A INPUT ! -i lo -p tcp -m state --state NEW -m tcp --dport 8085 -j ACCEPT


sudo iptables -A INPUT -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 443 -m conntrack --ctstate ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 80 -j ACCEPT

iptables -A INPUT -m state --state NEW -p tcp --dport 1024 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 2222 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 2222 -m conntrack --ctstate ESTABLISHED -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 2232 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 2232 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 1026 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 1026 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 1027 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 1027 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 1028 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 1028 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 1029 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 1029 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 1030 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 1030 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 3306 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 3306 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 8085 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 8085 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 8081 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 8081 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 6379 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 6379 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 1020:1050 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 1020:1050 -j ACCEPT

iptables -A INPUT -m state --state NEW -p tcp --dport 8843 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 8843 -j ACCEPT

iptables -A INPUT -m state --state NEW -p tcp --dport 11145 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 11145 -j ACCEPT

iptables -D INPUT -m state --state NEW -p tcp --dport 2222 -j ACCEPT
iptables -D OUTPUT -m state --state NEW -p tcp --dport 2222  -j ACCEPT
iptables -D INPUT -m state --state NEW -p tcp --dport 2224 -j ACCEPT
iptables -D OUTPUT -m state --state NEW -p tcp --dport 2224  -j ACCEPT
sudo iptables -D INPUT -p tcp --dport 2222 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -D OUTPUT -p tcp --sport 2222 -m conntrack --ctstate ESTABLISHED -j ACCEPT


iptables -A INPUT -m state --state NEW -p tcp --dport 4369 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 4369 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 8091 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 8091 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 9100 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 9100 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 9105 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 9105 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 9998 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 9998 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 9999 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 9999 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 11209 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 11209 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 11211 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 11211 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 11215 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 11215 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 18091 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 18093 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 21100:21299 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 21100:21299 -j ACCEPT